 
# WebReady
### Create web-ready animations in seconds
Use ffmpeg to convert your video files to looped WEBP animations. No extra bells or whistles. Open a file and press go. Made primarily for easily creating live wallpapers for the Steam Deck.

<img src=".screenshots/main.png" width="512">