#!/usr/bin/env bash

SRC_REPO="https://gitlab.com/nickgirga/webready.git"
INSTALL_DIR="$HOME/.local/share/com.nickgirga.webready"
APP_SHORTCUT_DIR="$HOME/.local/share/applications"
APP_SHORTCUT="$APP_SHORTCUT_DIR/com.nickgirga.webready.desktop"



zenity --question --ellipsize --text="Would you like to install WebReady?"
if [ "$?" == "1" ];
then
    echo Stopping installation \(user request\)
    exit
fi



# check for previous install
if [ -d "$INSTALL_DIR" ];
then
    echo WebReady appears to already be installed.
    zenity --info --ellipsize --text="WebReady appears to already be installed."
    exit
fi


# clone repo
zenity --info --ellipsize --text="Cloning repository..."
git clone $SRC_REPO "$INSTALL_DIR"
exit_code="$?"
if [ ! "$exit_code" == "0" ];
then
    echo ERROR!: Git returned with a non-zero exit code \($exit_code\)!
    zenity --error --ellipsize --text="Git could not clone the repository. Check terminal output for more information."
    exit 1
fi
zenity --info --ellipsize --text="Finished cloning repository!"

 
# update desktop file paths
sed -i "s+/home/deck/.local/share/com.nickgirga.webready+$HOME/.local/share/com.nickgirga.webready+g" "$INSTALL_DIR/WebReady.desktop"
exit_code="$?"
if [ ! "$exit_code" == "0" ];
then
    echo ERROR!: Could not update desktop file paths \(exit code $exit_code\)!
    zenity --error --ellipsize --text="Could not update desktop file paths! Check terminal output for more information."
    exit 2
fi
zenity --info --ellipsize --text="Updated desktop file paths!"


# check for previous desktop file link
if [ -f "$APP_SHORTCUT" ];
then
    echo A shortcut or link to WebReady appears to already exist in your applications folder.
    zenity --info --ellipsize --text="A shortcut or link to WebReady appears to exist in your applications folder."
    exit
fi


# create desktop file link
ln -s "$INSTALL_DIR/WebReady.desktop" "$APP_SHORTCUT"
exit_code="$?"
if [ ! "$exit_code" == "0" ];
then
    echo ERROR!: Could not create a link to the desktop file \(exit code $exit_code\)!
    zenity --error --ellipsize --text="Could not create a link to the desktop file. Check terminal output for more information."
    exit 3
fi
zenity --info --ellipsize --text="Created link to desktop file!"


echo Done!
zenity --info --ellipsize --text="Finished installing WebReady!"