#!/usr/bin/env bash

INSTALL_DIR="$HOME/.local/share/com.nickgirga.webready"
APP_SHORTCUT_DIR="$HOME/.local/share/applications"
APP_SHORTCUT="$APP_SHORTCUT_DIR/com.nickgirga.webready.desktop"



zenity --question --ellipsize --text="Would you like to uninstall WebReady?"
if [ "$?" == "1" ];
then
    echo Stopping installation \(user request\)
    exit
fi



# remove app resources
zenity --info --ellipsize --text="Removing app resources..."
if [ -d "$INSTALL_DIR" ];
then
    rm -rf "$INSTALL_DIR"
    exit_code="$?"
    if [ ! "$exit_code" == "0" ];
    then
        echo ERROR!: Could not delete application resources \(exit code $exit_code\)!
        exit 1
    fi
else
    echo WebReady is not installed.
fi
zenity --info --ellipsize --text="Finished removing app resources!"


# remove link to shortcut
if [ -L "$APP_SHORTCUT" ];
then
    rm -f "$APP_SHORTCUT"
    exit_code="$?"
    if [ ! "$exit_code" == "0" ];
    then
        echo ERROR!: Could not delete link to application \(exit code $exit_code\)!
        exit 1
    fi
fi
zenity --info --ellipsize --text="Removed app shortcut!"

echo Done!
zenity --info --ellipsize --text="Finished uninstalling WebReady!"